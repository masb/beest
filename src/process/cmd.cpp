#include <cstring>
#include <future>
#include <system_error>

#include <boost/process.hpp>

extern "C" {
#include <beest/process.h>

/* ----- */

namespace bp = boost::process;

/* ----- */

ret_t proc_exec_shell(outstr_t *out, proc_cmd_t const *cmd)
{
    ret_t ret;
    std::error_code ec;
    std::future<std::string> str;

    ret.val = bp::system(cmd->command, (bp::std_out & bp::std_err) > str, ec);
    ret.err = ec.value() != 0;

    if (out->cap != 0) {
        strncpy((char *)out->str, str.get().c_str(), out->cap);
    } else {
        out->str = strndup(str.get().c_str(), out->cap);
        if (out->str == NULL) { ret.err = ERR_ANY; }
    }

    return ret;
}

/* ----- */

}

