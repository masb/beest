#include <beest/error.h>

/* ----- */

void err_strerror(int errnum, char const **message)
{
    int ret = strerror_r(errnum, err_info.message, ERR_INFO_BUFSIZE);
    if (ret != 0) {
        char const *str = strerror(errnum);
        memmove(err_info.message, str, strlen(str));
    }
    if (message != NULL) {
        *message = err_info.message;
    }
}

void err_getmessage(char const **message)
{
    if (*err_info.message == '\0') {
        err_strerror(errno, message);
    } else {
        *message = err_info.message;
    }
}

void err_setmessage(char const *message)
{
    size_t n = strlen(message);

    if (n >= ERR_INFO_BUFSIZE) {
        // TODO: DEFINE ELLIPSIS LEN
        int pos = ERR_INFO_BUFSIZE - 4;
        memmove(err_info.message, message, pos);
        memmove(err_info.message + pos, "...", 4);
    } else {
        memmove(err_info.message, message, n);
    }
}
