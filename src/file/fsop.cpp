#include <boost/filesystem.hpp>
#include <boost/system/error_code.hpp>

extern "C" {
#include <beest/error.h>

/* ----- */

namespace fs = boost::filesystem;
namespace sys = boost::system;

/* ----- */

err_t file_copy(char const *from, char const *to)
{
    sys::error_code err;
    fs::copy(from, to, err);
    return err != sys::errc::success;
}

err_t file_rename(char const *from, char const *to)
{
    sys::error_code err;
    fs::rename(from, to, err);
    return err != sys::errc::success;
}

/* ----- */

}
