#include <cstring>

#include <boost/filesystem.hpp>
#include <boost/system/error_code.hpp>

extern "C" {
#include <beest/error.h>
#include <beest/output.h>

/* ----- */

namespace fs = boost::filesystem;
namespace sys = boost::system;

/* ----- */

err_t file_getcwd(outstr_t *path)
{
    sys::error_code err;
    auto p = fs::current_path(err);
    if (path->cap != 0) {
        strncpy((char *)path->str, p.c_str(), path->cap);
    } else {
        path->str = strndup(p.c_str(), path->cap);
        if (path->str == NULL) { return ERR_ANY; }
    }
    return err != sys::errc::success;
}

err_t file_chdir(char const *path)
{
    sys::error_code err;
    fs::current_path(path, err);
    return err != sys::errc::success;
}

/* ----- */

}
