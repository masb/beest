#include <cstdarg>
#include <iostream>
#include <sstream>

extern "C" {
#include <beest/error.h>

/* ----- */

typedef struct _io_istm_ {
    std::istream &stm;
} io_istm_t;

typedef struct _io_ostm_ {
    std::ostream &stm;
} io_ostm_t;

/* ----- */

io_istm_t io_cin = { std::cin };
io_ostm_t io_cout = { std::cout };

/* ----- */

uret32_t io_write(char const *str)
try {
    uret32_t ret;
    std::stringstream s;

    s << str << std::flush;
    ret.val = static_cast<uint32_t>(s.tellp() * sizeof(std::stringstream::char_type));
    std::cout << s.rdbuf() << std::flush;

    ret.err = ERR_NONE;
    return ret;
} catch (const std::ios_base::failure& e) {
    // e.what(); e.code();
    return errcast(uret32_t, ERR_ANY);
}

uret32_t io_writeln(char const *str)
try {
    uret32_t ret;
    std::stringstream s;

    s << str << std::endl;
    ret.val = static_cast<uint32_t>(s.tellp() * sizeof(std::stringstream::char_type));
    std::cout << s.rdbuf() << std::flush;

    ret.err = ERR_NONE;
    return ret;
} catch (const std::ios_base::failure& e) {
    // e.what(); e.code();
    return errcast(uret32_t, ERR_ANY);
}

/* ----- */

}
