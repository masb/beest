#pragma once
#ifndef __BEEST_VER_H__
#define __BEEST_VER_H__

/* ----- */

#define VER_NAME "beest"
#define VER_SEMVER "0.1.0"

/* ----- */

#endif /* !__BEEST_VER_H__ */
