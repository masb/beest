#pragma once
#ifndef __BEEST_FILE_H__
#define __BEEST_FILE_H__

#include <beest/error.h>
#include <beest/output.h>

/* ----- */

err_t file_getcwd(outstr_t *path);
err_t file_chdir(char const *path);

err_t file_copy(char const *from, char const *to);
err_t file_rename(char const *from, char const *to);

/* ----- */

#endif /* !__BEEST_FILE_H__ */
