#pragma once
#ifndef __BEEST_OUT_H__
#define __BEEST_OUT_H__

/* ----- */

typedef struct {
    char const *str;
    size_t cap;
} outstr_t;

/* ----- */

#define OUT_STR(str, cap) ((outstr_t){ str, cap })
#define OUT_STR_NULL ((outstr_t){ 0 })

/* ----- */

#endif /* !__BEEST_OUT_H__ */
