#pragma once
#ifndef __BEEST_IO_H__
#define __BEEST_IO_H__

#include <beest/error.h>

/* ----- */

typedef struct _io_istm_ io_istm_t;
typedef struct _io_ostm_ io_ostm_t;

/* ----- */

extern io_istm_t io_cin;
extern io_ostm_t io_cout;

/* ----- */

uret32_t io_write(char const *str, ...);
uret32_t io_writeln(char const *str, ...);

/* ----- */

#endif /* !__BEEST_IO_H__ */
