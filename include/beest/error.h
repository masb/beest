#pragma once
#ifndef __BEEST_ERR_H__
#define __BEEST_ERR_H__

#include <errno.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <threads.h>

/* ----- */

#define ERR_ANY true
#define ERR_NONE false

#define ERR_INFO_BUFSIZE 1024

/* ----- */

typedef bool err_t;

typedef struct {
    err_t err;
    int8_t val;
} ret8_t;

typedef struct {
    err_t err;
    int16_t val;
} ret16_t;

typedef struct {
    err_t err;
    int32_t val;
} ret32_t;

typedef struct {
    err_t err;
    int val;
} ret_t;

typedef struct {
    err_t err;
    uint8_t val;
} uret8_t;

typedef struct {
    err_t err;
    uint16_t val;
} uret16_t;

typedef struct {
    err_t err;
    uint32_t val;
} uret32_t;

typedef struct {
    err_t err;
    unsigned val;
} uret_t;

typedef struct {
    char message[ERR_INFO_BUFSIZE];
} err_info_t;

/* ----- */

#ifdef __BEEST_ERR_INFO__
#undef __BEEST_ERR_INFO__

static thread_local err_info_t err_info = { 0 };

#else /* __BEEST_ERR_INFO__ */

extern thread_local err_info_t err_info;

#endif /* __BEEST_ERR_INFO__ */

/* ----- */

#define errcast(type, x) ((type) { .err = x })
#define retcast(type, x) ((type) { .val = x })

/* ----- */

void err_strerror(int errnum, char const **message);
#define err_sync() err_strerror(errno, NULL)
void err_getmessage(char const **message);
void err_setmessage(char const *message);
#define err_retmessage(msg) (err_setmessage(msg), ERR_ANY)

/* ----- */

#endif /* !__BEEST_ERR_H__ */
