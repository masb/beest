#pragma once
#ifndef __BEEST_PROC_H__
#define __BEEST_PROC_H__

#include <beest/error.h>
#include <beest/output.h>

/* ----- */

typedef struct _proc_cmd_ {
    char const *command;
} proc_cmd_t;

/* ----- */

ret_t proc_exec_shell(outstr_t *out, proc_cmd_t const *cmd);

/* ----- */

#endif /* !__BEEST_PROC_H__ */
